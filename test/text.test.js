const request = require('supertest'),
    chai = require('chai'),
    expect = chai.expect,
    app = require('../server');

describe('post /text/reply', () => {
    it('should get a correct reply', (done) => {
        request(app)
            .post('/text/reply')
            .send({
                text: "Hello Toolbox"
            })
            .end((err, res) => {
                expect(res.body.status).to.eq(200);
                expect(res.body.reply).to.eq("Hello Toolbox");
                done()
            })
    })
    it('should get a bad reply', (done) => {
        request(app)
            .post('/text/reply')
            .send({
                text: 1
            })
            .end((err, res) => {
                expect(res.body.status).to.eq(500);
                expect(res.body.reply).to.eq("should be a text type field");
                done()
            })
    })
})