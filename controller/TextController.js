'use strict';
exports.reply = function (req, res) {
    let response = { status: 500, reply: "should be a text type field" };
    if (typeof req.body.text === "string") {
        response.status = 200;
        response.reply = req.body.text;
    }

    return res.json(response)
};
