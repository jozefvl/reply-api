# Reply API

- [Getting Started](#getting-started)
    * [Requirements](#requirements)
    * [Installation](#installation)
        + [Docker](#docker)
        + [Docker compose](#docker-compose)
- [Build and Run](#build-and-run)
- [Run Test](#run-test)
- [Execute Client](#execute-client)
    
## Getting Started

### Requirements

- Docker
- Docker compose

### Installation

#### Docker

- [Docker install steps](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

#### Docker Compose

- [Docker Compose install steps](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04)


## Build and Run

Go to your project folder and execute the following command:

```sh
$ sudo docker-compose up -d
```
You can check if the API status by:
```sh
$ sudo docker-compose ps
```
it will prompt something like this:
```sh
  Name           Command        State           Ports         
--------------------------------------------------------------
api-reply   forever server.js   Up      0.0.0.0:4000->3000/tcp
```
## Run Test
Once the API is up and running, open another terminal window and go to your project folder, then execute the following command:

```sh
$ npm test

```

## Execute Client
Once the API is up and running, open another terminal window and go to your project folder, then execute the following command:

```sh
$ node client.js "Hello Toolboox"

```