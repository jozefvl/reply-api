FROM node:8.15

RUN mkdir -p /var/www/app
WORKDIR /var/www/app

ADD package.json ./
RUN npm install

RUN npm install forever -g

ADD . /var/www/app

EXPOSE 3000

CMD ["forever", "server.js"]
