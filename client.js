var rp = require('request-promise');
var opt = {
    method: 'POST',
    uri: 'http://localhost:4000/text/reply',
    headers: {
        'Content-Type': 'application/json'
    },
    body: {
        text: process.argv[2]
    },
    json: true
};

rp(opt)
    .then(function (res) {
        console.log('msg: ', res.reply);
    })
    .catch(function (err) {
        console.log("err: ", err)
    });