'use strict';

var Text = require('../controller/TextController');

module.exports = function (app) {
    app.post('/text/reply', Text.reply);
};
